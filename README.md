# Box2D-Lite
Box2D-Lite is a small 2D physics engine. It was developed for the [2006 GDC Physics Tutorial](docs/GDC2006_Catto_Erin_PhysicsTutorial.pdf). This is the original version of the larger [Box2D](https://box2d.org) library. The Lite version is more suitable for learning about game physics.

# Building
- Install [CMake](https://cmake.org/)
- Ensure CMake is in the user `PATH`
- Visual Studio 2017: run `build.bat`
- Otherwise: run `build.sh` from a bash shell
- Results are in the build sub-folder

# About 
This work is part of the following research papers: 

`Ifrim, Rareş-Cristian, Patricia Penariu, and Costin-Anton Boiangiu. "REPLICATING IMPULSE-BASED PHYSICS ENGINE USING CLASSIC NEURAL NETWORKS." Journal of Information Systems & Operations Management 15, no. 2 (2021).`

`Avatavului, Cristian-Dumitru, Rareș-Cristian Ifrim, and Mihai Voncila. "Can Neural Networks Enhance Physics Simulations?." BRAIN. Broad Research in Artificial Intelligence and Neuroscience 14, no. 2 (2023): 76-92.`

The project builds upon Box2D-Lite by adding a simple Neural Network library which is trained to replicate the collision system of Box2D-Lite. Once trained, the neural network is used instead of the original system and analyzed to see how accurate  this appraoch can be compared to classic approaches such as modeling physics equation for the collisions.

To cite these works please use the following:
```
@article{ifrim2021replicating,
  title={REPLICATING IMPULSE-BASED PHYSICS ENGINE USING CLASSIC NEURAL NETWORKS.},
  author={Ifrim, Rare{\c{s}}-Cristian and Penariu, Patricia and Boiangiu, Costin-Anton},
  journal={Journal of Information Systems \& Operations Management},
  volume={15},
  number={2},
  year={2021}
}
```

```
@article{avatavului2023can,
  title={Can Neural Networks Enhance Physics Simulations?},
  author={Avatavului, Cristian-Dumitru and Ifrim, Rareș-Cristian and Voncila, Mihai},
  journal={BRAIN. Broad Research in Artificial Intelligence and Neuroscience},
  volume={14},
  number={2},
  pages={76--92},
  year={2023}
}
```
  
