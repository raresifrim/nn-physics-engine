#include "neuralnetwork.h"
#include "neuron.h"
#include "../misc/functions.h"


NeuralNetwork::NeuralNetwork()
{

}

void NeuralNetwork::load(string filename)
{
	ifstream in;
	in.open(filename);
    if (in.fail()) {
        cout << "ERROR:: CANNOT READ FROM FILE: '" << filename << "'\n";
        exit(1);
    }

	int num_layers;
	in >> num_layers;
	int *size = new int[num_layers];
	for(int i=0; i<num_layers;i++){
		double type, activation;
		in >> type >> size[i] >> activation;
		if(type == LayerType::OUTPUT)
			addLayer({{"type", type }, {"size", size[i]}, {"activation", activation}});
		else if(type == LayerType::INPUT)
			addLayer({{"type", type }, {"size", size[i]}});
		else
			addLayer({{"type", type }, {"size", size[i] - 1}, {"activation", activation}});
	}
	connectComplete();

	vector<vector<vector<double>>> weights;
	vector<vector<double>> neurons;
	string instance;
	int index;

	for(int i=0; i < num_layers - 1;i++){
		in >> index;
		if(index == i){
			neurons.clear();
			in >> ws;
			for(int j=0; j < size[i] ;j++){
				getline(in, instance);
				stringstream ss(instance);
				vector<double> w;
				double value;
				while(ss >> value){
					w.push_back(value);
				}
				neurons.push_back(w);
			}
		}
		weights.push_back(neurons);
	}

	in.close();
	alterWeights(weights);
}

NeuralNetwork::~NeuralNetwork()
{
    for(Layer* l : _layers)
        delete l;
}


void NeuralNetwork::autogenerate(bool randomize)
{
	connectComplete();
	if(randomize)
		randomizeAllWeights();
	
}

void NeuralNetwork::addLayer(unordered_map<string, double> parameters)
{
	_layers.push_back(new Layer(_layers.size(), this, parameters));
}


void NeuralNetwork::clean()
{
    for(Layer* l : _layers)
        l->clean();
}

void NeuralNetwork::setInput(vector<double> in)
{
	clean();
    for(size_t i=0; i<in.size(); ++i)
        _layers[0]->neurons()[i]->setAccumulated(in[i]);
}

void NeuralNetwork::trigger()
{
	for (Layer* l : _layers)
		l->trigger();
}

vector<double> NeuralNetwork::output()
{
    return (_layers.back())->output();
}

string NeuralNetwork::outputString()
{
	string s;
	for (size_t i = 0; i < (_layers.back())->output().size(); i++)
	{
		double out = (_layers.back())->output()[i];
		s += to_string(out) + " ";
	}
	return s;
}

void NeuralNetwork::connectComplete()
{
    for(int i_layer = 0; i_layer < _layers.size()-1; ++i_layer)
		_layers[i_layer]->connectComplete(_layers[i_layer+1]);
}

void NeuralNetwork::alterWeights(const vector<vector<vector<double> > >& weights)
{
	for (int i_layer = 0; i_layer < _layers.size() - 1; ++i_layer)
		_layers[i_layer]->alterWeights(weights[i_layer]);
}

void NeuralNetwork::shiftBackWeights(const vector<vector<vector<double> > >& weights)
{
	for (int i_layer = _layers.size() - 1; i_layer >= 0; --i_layer)
		if(weights[i_layer].size() != 0)
			_layers[i_layer]->shiftBackWeights(weights[i_layer]);
}

vector<vector<vector<double*>>> NeuralNetwork::getWeights()
{
	vector<vector<vector<double*>>> w;
	w.reserve(_layers.size() - 1);
	for (int i_layer = 0; i_layer < _layers.size() - 1; ++i_layer)
		w.push_back(std::move(_layers[i_layer]->getWeights()));
	return std::move(w);
}

vector<vector<vector<Edge*>>> NeuralNetwork::getEdges()
{
	vector<vector<vector<Edge*>>> w;
	w.reserve(_layers.size() - 1);
	for (int i_layer = 0; i_layer < _layers.size() - 1; ++i_layer)
		w.push_back(std::move(_layers[i_layer]->getEdges()));
	return std::move(w);
}

void NeuralNetwork::randomizeAllWeights()
{
	for(int i_layer = 0; i_layer < _layers.size() - 1; ++i_layer)
		_layers[i_layer]->randomizeAllWeights(RAND_MAX_WEIGHT); //random weights from -RAND_MAX_WEIGHT to RAND_MAX_WEIGHT
}

double NeuralNetwork::loss(const vector<double>& in, const vector<double>& out)
{
	double sum = 0;
	auto out_exp = predict(in);
	if (_layers.back()->getParameters().at("activation") == ActivationFunction::SIGMOID)
		for (size_t i = 0; i < out.size(); ++i)
			sum += 0.5 * (out[i] - out_exp[i]) * (out[i] - out_exp[i]);
	if (_layers.back()->getParameters().at("activation") == ActivationFunction::LINEAR)
		for (size_t i = 0; i < out.size(); ++i)
			sum += (out[i] - out_exp[i]) * (out[i] - out_exp[i]);
	return sum;
}

double NeuralNetwork::loss(const vector<vector<double>*>& ins, const vector<vector<double>*>& outs)
{
	double sum = 0;
	for (size_t i = 0; i < ins.size(); i++)
	{
		sum += loss(*ins[i], *outs[i]);
	}
	return sum / ins.size() ;
}

string NeuralNetwork::toString()
{
    string s = "";
    for(Layer* l : _layers)
        s += l->toString();
    return s;
}


void NeuralNetwork::shiftWeights(float percentage_of_range)
{
	float range = percentage_of_range * (RAND_MAX_WEIGHT + RAND_MAX_WEIGHT); //distance entre min et max des poids
	for(Layer* l : _layers)
		l->shiftWeights(range);
}


vector<double> NeuralNetwork::predict(const vector<double>& in)
{
	setInput(in);
	trigger();
	return output();
}

double NeuralNetwork::predictAllForScore(const Dataset& dataset, Datatype d,  int limit)
{
	if (limit == 0)
		return 1;
	double s = 0;

	//Sans limite explicite, on score toutes les donn�es
	if (limit == -1)
		for (size_t i = 0; i < dataset.getIns(d).size(); i++)
			s += distanceVector(predict(*dataset.getIns(d)[i]), *dataset.getOuts(d)[i]);
	//Sinon on prend "limit" donn�es
	else
		for (size_t i = 0; i < limit; i++)
		{
			int r = rand() % dataset.getIns(d).size();
			s += distanceVector(predict(*dataset.getIns(d)[r]), *dataset.getOuts(d)[r]);
		}

	//On moyenne le score
	if (limit == -1)
		s /= dataset.getIns(d).size();
	else
		s /= limit;
	return s;
}

vector<Layer*> NeuralNetwork::getLayers()
{
	return _layers;
}

void NeuralNetwork::save(string filename){
	std::ofstream in(filename);
    if (in.fail()) {
        std::cout << "ERROR:: CANNOT READ FROM FILE: '" << filename << "'\n";
        exit(1);
    }
	vector<Layer*> layers = getLayers();
	in << layers.size() <<endl;
	for(int i=0; i<layers.size();i++){
		in << layers[i]->getParameters().at("type") << " "
		   << layers[i]->getParameters().at("size") << " "
		   << layers[i]->getParameters().at("activation") << endl;
	}
	string weights = toString();
	in << weights;
	in.close();
}

