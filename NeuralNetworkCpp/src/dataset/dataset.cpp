#include "dataset.h"

#include <fstream>
#include <algorithm>
#include "../misc/functions.h"

std::string exec(std::string command) {
   char buffer[128];
   std::string result = "";

   // Open pipe to file
   FILE* pipe = popen(command.c_str(), "r");
   if (!pipe) {
      return "popen failed!";
   }

   // read till end of process:
   while (!feof(pipe)) {

      // use buffer to read and add to result
      if (fgets(buffer, 128, pipe) != NULL)
         result += buffer;
   }

   pclose(pipe);
   return result;
}

Dataset::Dataset(string filename)
{
    std::ifstream in(filename);
    if (in.fail()) {
        std::cout << "ERROR:: CANNOT READ FROM FILE: '" << filename << "'\n";
        exit(1);
    }
    
    double min_x = std::atof(exec("cut -f11 -d\" \" " + filename+ " | sort -n | head -1").c_str());
    double min_y = std::atof(exec("cut -f12 -d\" \" " + filename+ " | sort -n | head -1").c_str());
    double max_x = std::atof(exec("cut -f11 -d\" \" " + filename+ " | sort -n | tail -1").c_str());
    double max_y = std::atof(exec("cut -f12 -d\" \" " + filename+ " | sort -n | tail -1").c_str());
    cout << max_x << " " << min_x << ";" << max_y << " " << min_y <<endl;

    std::string instance;
    while (getline(in, instance)) {
        std::stringstream ss(instance);
        double value;
        
        vector<double> line_input;
    	vector<double> line_output;
        size_t count = 0;
        while (ss >> value) {
            if(count < 10)
              line_input.push_back(value);
            if(count == 10)
               line_output.push_back((value-min_x)/(max_x-min_x));
            if(count == 11)
               line_output.push_back((value-min_y)/(max_y-min_y));
            ++count;
        }
      _ins.push_back(line_input);
      _outs.push_back(line_output);
    }
}

Dataset::~Dataset()
{
}

void Dataset::split(double ptrain)
{
	for (size_t i = 0; i < _ins.size(); i++)
	{
		if (random(0, 1) < ptrain)
		{
			_train_ins.push_back(&_ins[i]);
			_train_outs.push_back(&_outs[i]);
		}
		else
		{
			_test_ins.push_back(&_ins[i]);
			_test_outs.push_back(&_outs[i]);
		}
	}
}


const vector<const vector<double>*>& Dataset::getIns(Datatype d) const
{ 
	if(d == Datatype::TRAIN)
		return _train_ins;
	return _test_ins;
}

const vector<const vector<double>*>& Dataset::getOuts(Datatype d) const
{
	if (d == Datatype::TRAIN)
		return _train_outs;
	return _test_outs;
}





