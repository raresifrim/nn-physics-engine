#include "box2d-lite/NeuronalNetwork.h"
#include <iostream> 
#include <fstream>

void genData(std::vector<double> f_in, std::string filename) 
{ 
    std::ofstream file(filename + ".txt", std::ios::app);  
    
    for(int i=0;i<f_in.size();i++)     
        file << f_in[i] << " ";
    file << std::endl; 
    
    file.close(); 
} 
